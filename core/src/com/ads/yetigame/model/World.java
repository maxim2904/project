package com.ads.yetigame.model;

import com.ads.yetigame.Controller.MoveToMainGamescreen;
import com.ads.yetigame.Controller.MoveToMainMenu;
import com.ads.yetigame.Controller.StageController;
import com.ads.yetigame.YetiGame;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.scenes.scene2d.Stage;

import java.io.File;
import java.io.IOException;
import java.util.Random;


/**
 * Created by Максим on 07.03.2016.
 */
public class World extends Stage {
    public World world;
    private InputMultiplexer multiplexer;
    public int[][] field;
    public int cellSize;
    private ShapeRenderer shape;
    private BitmapFont font;
    Vector2 leftCorner;
    public int Score;
    private GlyphLayout glyph;

    public World(int n, ScreenViewport viewport, Batch batch) {
        super(viewport, batch);
        Random rnd = new Random();
        shape = YetiGame.getInstance().getShapeRenderer();
        font = YetiGame.getInstance().getFont();
        cellSize = Math.min(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()) / n;
        leftCorner = new Vector2((Gdx.graphics.getWidth() - Math.min(Gdx.graphics.getWidth(), Gdx.graphics.getHeight())) / 2,
                (Gdx.graphics.getHeight() - Math.min(Gdx.graphics.getWidth(), Gdx.graphics.getHeight())) / 3);
        field = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                int p = rnd.nextInt(8);
                if (p == 0 || p == 1 || p == 3 || p == 5 || p == 6 || p == 7) {
                    p = 0;
                }
                field[i][j] = p;




            }
        }

        YetiGame.getInstance().file = new File("android\\assets\\HighScore.txt");
        if (!YetiGame.getInstance().file.exists())
            try {
                YetiGame.getInstance().setHighScore();
            } catch (IOException e) {
                e.printStackTrace();
            }
        glyph = new GlyphLayout();

    }


    public void draw() {
        super.draw();
        shape.setProjectionMatrix(getBatch().getProjectionMatrix());
        shape.begin(ShapeRenderer.ShapeType.Filled);
        shape.setColor(192f / 255f, 168f / 255f, 140f / 255f, 1f);




        for (int i = 0; i < field.length; i++)
            for (int j = 0; j < field.length; j++)
                shape.rect(leftCorner.x + cellSize * i + 4, leftCorner.y + cellSize * j + 4, cellSize - 8, cellSize - 8);

        shape.end();
        getBatch().begin();

        for (int i = 0; i < field.length; i++)
            for (int j = 0; j < field.length; j++)
                if (field[i][j] != 0){
                    glyph.setText(font, String.format("%d", field[i][j]));
                    font.draw(getBatch(), glyph, leftCorner.x + cellSize * i+ cellSize / 2 - glyph.width/2, leftCorner.y + cellSize * j + cellSize / 2 + glyph.height/2);

//                    font.draw(getBatch(), String.format("%d", field[i][j]), leftCorner.x + cellSize * i + cellSize / 2 - 18, leftCorner.y + cellSize * j + cellSize / 2 + 20);
                }

        getBatch().end();
    }

    public boolean canMove() {
        for (int i = 0; i < field.length; i++)
            for (int j = 0; j < field[i].length; j++)
                if (field[i][j] == 0)
                    return true;
        for (int i = 1; i < field.length - 1; i++)
            for (int j = 1; j < field[i].length - 1; j++) {
                if (field[i][j] == field[i + 1][j])
                    return true;
                if (field[i][j] == field[i - 1][j])
                    return true;
                if (field[i][j] == field[i][j + 1])
                    return true;
                if (field[i][j] == field[i][j - 1])
                    return true;
            }

        return false;
    }

    public int summ = 1;

    public void moveLeft() {
        if (canMove() == true) {
            summ += 2;
            System.out.println("Left");









           for (int i = 0; i < field.length; i++) {
               // for (int j = 0; j < field[i].length; j++) {
                    //i - строка
                    //j - столбец
                    int pv = 0;
                    int row = 1;
                    while (row < field.length) {

                        if (field[row][i] == 0) {
                            row++;

                        }  else if (field[pv][i] == field[row][i]) {
                                field[pv++][i] += field[row][i];
                                field[row++][i] = 0;

                        } else if (field[pv][i] == 0) {
                            field[pv][i] = field[row][i];
                            field[row++][i] = 0;



                        } else if (++pv == row) {
                            row++;
                        }
                    }


            }
            //region Field 4x4 Random
            Random rnd = new Random();
            int p = rnd.nextInt(5);
            int k = rnd.nextInt(4);
            int r = rnd.nextInt(4);
            if (p == 0 || p == 1 || p == 3) {
                p = 2;

            }

            if(field.length==4) {
                if (field[k][r] == 0) field[k][r] = p;
                else if (field[0][0] == 0) field[0][0] = p;
                else if (field[0][1] == 0) field[0][1] = p;
                else if (field[0][2] == 0) field[0][2] = p;
                else if (field[0][3] == 0) field[0][3] = p;
                else if (field[1][0] == 0) field[1][0] = p;
                else if (field[1][1] == 0) field[1][1] = p;
                else if (field[1][2] == 0) field[1][2] = p;
                else if (field[1][3] == 0) field[1][3] = p;
                else if (field[2][0] == 0) field[2][0] = p;
                else if (field[2][1] == 0) field[2][1] = p;
                else if (field[2][2] == 0) field[2][2] = p;
                else if (field[2][3] == 0) field[2][3] = p;
                else if (field[3][0] == 0) field[3][0] = p;
                else if (field[3][1] == 0) field[3][1] = p;
                else if (field[3][2] == 0) field[3][2] = p;
                else if (field[3][3] == 0) field[3][3] = p;
            }
            //endregion
            //region Field 6x6 Random
            if(field.length==6) {
                if (field[k][r] == 0)
                    field[k][r] = p;
                else if (field[0][0] == 0) field[0][0] = p;
                else if (field[0][1] == 0) field[0][1] = p;
                else if (field[0][2] == 0) field[0][2] = p;
                else if (field[0][3] == 0) field[0][3] = p;
                else if (field[0][4] == 0) field[0][4] = p;
                else if (field[0][5] == 0) field[0][5] = p;


                else if (field[1][0] == 0) field[1][0] = p;
                else if (field[1][1] == 0) field[1][1] = p;
                else if (field[1][2] == 0) field[1][2] = p;
                else if (field[1][3] == 0) field[1][3] = p;
                else if (field[1][4] == 0) field[1][4] = p;
                else if (field[1][5] == 0) field[1][5] = p;


                else if (field[2][0] == 0) field[2][0] = p;
                else if (field[2][1] == 0) field[2][1] = p;
                else if (field[2][2] == 0) field[2][2] = p;
                else if (field[2][3] == 0) field[2][3] = p;
                else if (field[2][4] == 0) field[2][4] = p;
                else if (field[2][5] == 0) field[2][5] = p;



                else if (field[3][0] == 0) field[3][0] = p;
                else if (field[3][1] == 0) field[3][1] = p;
                else if (field[3][2] == 0) field[3][2] = p;
                else if (field[3][3] == 0) field[3][3] = p;
                else if (field[3][4] == 0) field[3][4] = p;
                else if (field[3][5] == 0) field[3][5] = p;


                else if (field[4][0] == 0) field[4][0] = p;
                else if (field[4][1] == 0) field[4][1] = p;
                else if (field[4][2] == 0) field[4][2] = p;
                else if (field[4][3] == 0) field[4][3] = p;
                else if (field[4][4] == 0) field[4][4] = p;
                else if (field[4][5] == 0) field[4][5] = p;


                else if (field[5][0] == 0) field[5][0] = p;
                else if (field[5][1] == 0) field[5][1] = p;
                else if (field[5][2] == 0) field[5][2] = p;
                else if (field[5][3] == 0) field[5][3] = p;
                else if (field[5][4] == 0) field[5][4] = p;
                else if (field[5][5] == 0) field[5][5] = p;


            }
            //endregion
            //region Field 8x8 Random
            if(field.length==8) {
                if (field[k][r] == 0)
                    field[k][r] = p;
                else if (field[0][0] == 0) field[0][0] = p;
                else if (field[0][1] == 0) field[0][1] = p;
                else if (field[0][2] == 0) field[0][2] = p;
                else if (field[0][3] == 0) field[0][3] = p;
                else if (field[0][4] == 0) field[0][4] = p;
                else if (field[0][5] == 0) field[0][5] = p;
                else if (field[0][6] == 0) field[0][6] = p;
                else if (field[0][7] == 0) field[0][7] = p;


                else if (field[1][0] == 0) field[1][0] = p;
                else if (field[1][1] == 0) field[1][1] = p;
                else if (field[1][2] == 0) field[1][2] = p;
                else if (field[1][3] == 0) field[1][3] = p;
                else if (field[1][4] == 0) field[1][4] = p;
                else if (field[1][5] == 0) field[1][5] = p;
                else if (field[1][6] == 0) field[1][6] = p;
                else if (field[1][7] == 0) field[1][7] = p;


                else if (field[2][0] == 0) field[2][0] = p;
                else if (field[2][1] == 0) field[2][1] = p;
                else if (field[2][2] == 0) field[2][2] = p;
                else if (field[2][3] == 0) field[2][3] = p;
                else if (field[2][4] == 0) field[2][4] = p;
                else if (field[2][5] == 0) field[2][5] = p;
                else if (field[2][6] == 0) field[2][6] = p;
                else if (field[2][7] == 0) field[2][7] = p;


                else if (field[3][0] == 0) field[3][0] = p;
                else if (field[3][1] == 0) field[3][1] = p;
                else if (field[3][2] == 0) field[3][2] = p;
                else if (field[3][3] == 0) field[3][3] = p;
                else if (field[3][4] == 0) field[3][4] = p;
                else if (field[3][5] == 0) field[3][5] = p;
                else if (field[3][6] == 0) field[3][6] = p;
                else if (field[3][7] == 0) field[3][7] = p;


                else if (field[4][0] == 0) field[4][0] = p;
                else if (field[4][1] == 0) field[4][1] = p;
                else if (field[4][2] == 0) field[4][2] = p;
                else if (field[4][3] == 0) field[4][3] = p;
                else if (field[4][4] == 0) field[4][4] = p;
                else if (field[4][5] == 0) field[4][5] = p;
                else if (field[4][6] == 0) field[4][6] = p;
                else if (field[4][7] == 0) field[4][7] = p;


                else if (field[5][0] == 0) field[5][0] = p;
                else if (field[5][1] == 0) field[5][1] = p;
                else if (field[5][2] == 0) field[5][2] = p;
                else if (field[5][3] == 0) field[5][3] = p;
                else if (field[5][4] == 0) field[5][4] = p;
                else if (field[5][5] == 0) field[5][5] = p;
                else if (field[5][6] == 0) field[5][6] = p;
                else if (field[5][7] == 0) field[5][7] = p;


                else if (field[6][0] == 0) field[6][0] = p;
                else if (field[6][1] == 0) field[6][1] = p;
                else if (field[6][2] == 0) field[6][2] = p;
                else if (field[6][3] == 0) field[6][3] = p;
                else if (field[6][4] == 0) field[6][4] = p;
                else if (field[6][5] == 0) field[6][5] = p;
                else if (field[6][6] == 0) field[6][6] = p;
                else if (field[6][7] == 0) field[6][7] = p;


                else if (field[7][0] == 0) field[7][0] = p;
                else if (field[7][1] == 0) field[7][1] = p;
                else if (field[7][2] == 0) field[7][2] = p;
                else if (field[7][3] == 0) field[7][3] = p;
                else if (field[7][4] == 0) field[7][4] = p;
                else if (field[7][5] == 0) field[7][5] = p;
                else if (field[7][6] == 0) field[7][6] = p;
                else if (field[7][7] == 0) field[7][7] = p;
            }
            //endregion
            //region Field 10x10 Random
            if(field.length==10) {
                if (field[k][r] == 0)
                    field[k][r] = p;
                else if (field[0][0] == 0) field[0][0] = p;
                else if (field[0][1] == 0) field[0][1] = p;
                else if (field[0][2] == 0) field[0][2] = p;
                else if (field[0][3] == 0) field[0][3] = p;
                else if (field[0][4] == 0) field[0][4] = p;
                else if (field[0][5] == 0) field[0][5] = p;
                else if (field[0][6] == 0) field[0][6] = p;
                else if (field[0][7] == 0) field[0][7] = p;
                else if (field[0][8] == 0) field[0][8] = p;
                else if (field[0][9] == 0) field[0][9] = p;


                else if (field[1][0] == 0) field[1][0] = p;
                else if (field[1][1] == 0) field[1][1] = p;
                else if (field[1][2] == 0) field[1][2] = p;
                else if (field[1][3] == 0) field[1][3] = p;
                else if (field[1][4] == 0) field[1][4] = p;
                else if (field[1][5] == 0) field[1][5] = p;
                else if (field[1][6] == 0) field[1][6] = p;
                else if (field[1][7] == 0) field[1][7] = p;
                else if (field[1][8] == 0) field[1][8] = p;
                else if (field[1][9] == 0) field[1][9] = p;


                else if (field[2][0] == 0) field[2][0] = p;
                else if (field[2][1] == 0) field[2][1] = p;
                else if (field[2][2] == 0) field[2][2] = p;
                else if (field[2][3] == 0) field[2][3] = p;
                else if (field[2][4] == 0) field[2][4] = p;
                else if (field[2][5] == 0) field[2][5] = p;
                else if (field[2][6] == 0) field[2][6] = p;
                else if (field[2][7] == 0) field[2][7] = p;
                else if (field[2][8] == 0) field[2][8] = p;
                else if (field[2][9] == 0) field[2][9] = p;


                else if (field[3][0] == 0) field[3][0] = p;
                else if (field[3][1] == 0) field[3][1] = p;
                else if (field[3][2] == 0) field[3][2] = p;
                else if (field[3][3] == 0) field[3][3] = p;
                else if (field[3][4] == 0) field[3][4] = p;
                else if (field[3][5] == 0) field[3][5] = p;
                else if (field[3][6] == 0) field[3][6] = p;
                else if (field[3][7] == 0) field[3][7] = p;
                else if (field[3][8] == 0) field[3][8] = p;
                else if (field[3][9] == 0) field[3][9] = p;


                else if (field[4][0] == 0) field[4][0] = p;
                else if (field[4][1] == 0) field[4][1] = p;
                else if (field[4][2] == 0) field[4][2] = p;
                else if (field[4][3] == 0) field[4][3] = p;
                else if (field[4][4] == 0) field[4][4] = p;
                else if (field[4][5] == 0) field[4][5] = p;
                else if (field[4][6] == 0) field[4][6] = p;
                else if (field[4][7] == 0) field[4][7] = p;
                else if (field[4][8] == 0) field[4][8] = p;
                else if (field[4][9] == 0) field[4][9] = p;


                else if (field[5][0] == 0) field[5][0] = p;
                else if (field[5][1] == 0) field[5][1] = p;
                else if (field[5][2] == 0) field[5][2] = p;
                else if (field[5][3] == 0) field[5][3] = p;
                else if (field[5][4] == 0) field[5][4] = p;
                else if (field[5][5] == 0) field[5][5] = p;
                else if (field[5][6] == 0) field[5][6] = p;
                else if (field[5][7] == 0) field[5][7] = p;
                else if (field[5][8] == 0) field[5][8] = p;
                else if (field[5][9] == 0) field[5][9] = p;


                else if (field[6][0] == 0) field[6][0] = p;
                else if (field[6][1] == 0) field[6][1] = p;
                else if (field[6][2] == 0) field[6][2] = p;
                else if (field[6][3] == 0) field[6][3] = p;
                else if (field[6][4] == 0) field[6][4] = p;
                else if (field[6][5] == 0) field[6][5] = p;
                else if (field[6][6] == 0) field[6][6] = p;
                else if (field[6][7] == 0) field[6][7] = p;
                else if (field[6][8] == 0) field[6][8] = p;
                else if (field[6][9] == 0) field[6][9] = p;


                else if (field[7][0] == 0) field[7][0] = p;
                else if (field[7][1] == 0) field[7][1] = p;
                else if (field[7][2] == 0) field[7][2] = p;
                else if (field[7][3] == 0) field[7][3] = p;
                else if (field[7][4] == 0) field[7][4] = p;
                else if (field[7][5] == 0) field[7][5] = p;
                else if (field[7][6] == 0) field[7][6] = p;
                else if (field[7][7] == 0) field[7][7] = p;
                else if (field[7][8] == 0) field[7][8] = p;
                else if (field[7][9] == 0) field[7][9] = p;


                else if (field[8][0] == 0) field[8][0] = p;
                else if (field[8][1] == 0) field[8][1] = p;
                else if (field[8][2] == 0) field[8][2] = p;
                else if (field[8][3] == 0) field[8][3] = p;
                else if (field[8][4] == 0) field[8][4] = p;
                else if (field[8][5] == 0) field[8][5] = p;
                else if (field[8][6] == 0) field[8][6] = p;
                else if (field[8][7] == 0) field[8][7] = p;
                else if (field[8][8] == 0) field[8][8] = p;
                else if (field[8][9] == 0) field[8][9] = p;


                else if (field[9][0] == 0) field[9][0] = p;
                else if (field[9][1] == 0) field[9][1] = p;
                else if (field[9][2] == 0) field[9][2] = p;
                else if (field[9][3] == 0) field[9][3] = p;
                else if (field[9][4] == 0) field[9][4] = p;
                else if (field[9][5] == 0) field[9][5] = p;
                else if (field[9][6] == 0) field[9][6] = p;
                else if (field[9][7] == 0) field[9][7] = p;
                else if (field[9][8] == 0) field[9][8] = p;
                else if (field[9][9] == 0) field[9][9] = p;
            }
            //endregion

            System.out.println();
            System.out.println(summ);
        } else {
            System.out.println("Erroe");
            try {

                gameOver();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        YetiGame.getInstance().setScore(summ);
    }

    public void moveRight() {
        if (canMove() == true) {
            System.out.println("Right");
            summ += 2;
            for (int i = 0; i < field.length; i++) {
               // for (int j = 0; j < field[i].length; j++) {


                    //i - строка
                    //j - столбец
                    int pv = field.length - 1;
                    int row = field.length - 2;
                    while (row >= 0) {
                        if (field[row][i] == 0)
                            row--;


                        else if (field[pv][i] == 0) {
                            field[pv][i] = field[row][i];
                            field[row--][i] = 0;

                        } else if (field[pv][i] == field[row][i]) {
                            field[pv--][i] += field[row][i];
                            field[row--][i] = 0;
                        } else if (--pv == row)
                            row--;

                    }



            }
            //region Field 4x4 Random
            Random rnd = new Random();
            int p = rnd.nextInt(5);
            int k = rnd.nextInt(4);
            int r = rnd.nextInt(4);
            if (p == 0 || p == 1 || p == 3) {
                p = 2;

            }

            if(field.length==4) {
                if (field[k][r] == 0) field[k][r] = p;
                else if (field[0][0] == 0) field[0][0] = p;
                else if (field[0][1] == 0) field[0][1] = p;
                else if (field[0][2] == 0) field[0][2] = p;
                else if (field[0][3] == 0) field[0][3] = p;
                else if (field[1][0] == 0) field[1][0] = p;
                else if (field[1][1] == 0) field[1][1] = p;
                else if (field[1][2] == 0) field[1][2] = p;
                else if (field[1][3] == 0) field[1][3] = p;
                else if (field[2][0] == 0) field[2][0] = p;
                else if (field[2][1] == 0) field[2][1] = p;
                else if (field[2][2] == 0) field[2][2] = p;
                else if (field[2][3] == 0) field[2][3] = p;
                else if (field[3][0] == 0) field[3][0] = p;
                else if (field[3][1] == 0) field[3][1] = p;
                else if (field[3][2] == 0) field[3][2] = p;
                else if (field[3][3] == 0) field[3][3] = p;
            }
            //endregion
            //region Field 6x6 Random
            if(field.length==6) {
                if (field[k][r] == 0)
                    field[k][r] = p;
                else if (field[0][0] == 0) field[0][0] = p;
                else if (field[0][1] == 0) field[0][1] = p;
                else if (field[0][2] == 0) field[0][2] = p;
                else if (field[0][3] == 0) field[0][3] = p;
                else if (field[0][4] == 0) field[0][4] = p;
                else if (field[0][5] == 0) field[0][5] = p;


                else if (field[1][0] == 0) field[1][0] = p;
                else if (field[1][1] == 0) field[1][1] = p;
                else if (field[1][2] == 0) field[1][2] = p;
                else if (field[1][3] == 0) field[1][3] = p;
                else if (field[1][4] == 0) field[1][4] = p;
                else if (field[1][5] == 0) field[1][5] = p;


                else if (field[2][0] == 0) field[2][0] = p;
                else if (field[2][1] == 0) field[2][1] = p;
                else if (field[2][2] == 0) field[2][2] = p;
                else if (field[2][3] == 0) field[2][3] = p;
                else if (field[2][4] == 0) field[2][4] = p;
                else if (field[2][5] == 0) field[2][5] = p;



                else if (field[3][0] == 0) field[3][0] = p;
                else if (field[3][1] == 0) field[3][1] = p;
                else if (field[3][2] == 0) field[3][2] = p;
                else if (field[3][3] == 0) field[3][3] = p;
                else if (field[3][4] == 0) field[3][4] = p;
                else if (field[3][5] == 0) field[3][5] = p;


                else if (field[4][0] == 0) field[4][0] = p;
                else if (field[4][1] == 0) field[4][1] = p;
                else if (field[4][2] == 0) field[4][2] = p;
                else if (field[4][3] == 0) field[4][3] = p;
                else if (field[4][4] == 0) field[4][4] = p;
                else if (field[4][5] == 0) field[4][5] = p;


                else if (field[5][0] == 0) field[5][0] = p;
                else if (field[5][1] == 0) field[5][1] = p;
                else if (field[5][2] == 0) field[5][2] = p;
                else if (field[5][3] == 0) field[5][3] = p;
                else if (field[5][4] == 0) field[5][4] = p;
                else if (field[5][5] == 0) field[5][5] = p;


            }
            //endregion
            //region Field 8x8 Random
            if(field.length==8) {
                if (field[k][r] == 0)
                    field[k][r] = p;
                else if (field[0][0] == 0) field[0][0] = p;
                else if (field[0][1] == 0) field[0][1] = p;
                else if (field[0][2] == 0) field[0][2] = p;
                else if (field[0][3] == 0) field[0][3] = p;
                else if (field[0][4] == 0) field[0][4] = p;
                else if (field[0][5] == 0) field[0][5] = p;
                else if (field[0][6] == 0) field[0][6] = p;
                else if (field[0][7] == 0) field[0][7] = p;


                else if (field[1][0] == 0) field[1][0] = p;
                else if (field[1][1] == 0) field[1][1] = p;
                else if (field[1][2] == 0) field[1][2] = p;
                else if (field[1][3] == 0) field[1][3] = p;
                else if (field[1][4] == 0) field[1][4] = p;
                else if (field[1][5] == 0) field[1][5] = p;
                else if (field[1][6] == 0) field[1][6] = p;
                else if (field[1][7] == 0) field[1][7] = p;


                else if (field[2][0] == 0) field[2][0] = p;
                else if (field[2][1] == 0) field[2][1] = p;
                else if (field[2][2] == 0) field[2][2] = p;
                else if (field[2][3] == 0) field[2][3] = p;
                else if (field[2][4] == 0) field[2][4] = p;
                else if (field[2][5] == 0) field[2][5] = p;
                else if (field[2][6] == 0) field[2][6] = p;
                else if (field[2][7] == 0) field[2][7] = p;


                else if (field[3][0] == 0) field[3][0] = p;
                else if (field[3][1] == 0) field[3][1] = p;
                else if (field[3][2] == 0) field[3][2] = p;
                else if (field[3][3] == 0) field[3][3] = p;
                else if (field[3][4] == 0) field[3][4] = p;
                else if (field[3][5] == 0) field[3][5] = p;
                else if (field[3][6] == 0) field[3][6] = p;
                else if (field[3][7] == 0) field[3][7] = p;


                else if (field[4][0] == 0) field[4][0] = p;
                else if (field[4][1] == 0) field[4][1] = p;
                else if (field[4][2] == 0) field[4][2] = p;
                else if (field[4][3] == 0) field[4][3] = p;
                else if (field[4][4] == 0) field[4][4] = p;
                else if (field[4][5] == 0) field[4][5] = p;
                else if (field[4][6] == 0) field[4][6] = p;
                else if (field[4][7] == 0) field[4][7] = p;


                else if (field[5][0] == 0) field[5][0] = p;
                else if (field[5][1] == 0) field[5][1] = p;
                else if (field[5][2] == 0) field[5][2] = p;
                else if (field[5][3] == 0) field[5][3] = p;
                else if (field[5][4] == 0) field[5][4] = p;
                else if (field[5][5] == 0) field[5][5] = p;
                else if (field[5][6] == 0) field[5][6] = p;
                else if (field[5][7] == 0) field[5][7] = p;


                else if (field[6][0] == 0) field[6][0] = p;
                else if (field[6][1] == 0) field[6][1] = p;
                else if (field[6][2] == 0) field[6][2] = p;
                else if (field[6][3] == 0) field[6][3] = p;
                else if (field[6][4] == 0) field[6][4] = p;
                else if (field[6][5] == 0) field[6][5] = p;
                else if (field[6][6] == 0) field[6][6] = p;
                else if (field[6][7] == 0) field[6][7] = p;


                else if (field[7][0] == 0) field[7][0] = p;
                else if (field[7][1] == 0) field[7][1] = p;
                else if (field[7][2] == 0) field[7][2] = p;
                else if (field[7][3] == 0) field[7][3] = p;
                else if (field[7][4] == 0) field[7][4] = p;
                else if (field[7][5] == 0) field[7][5] = p;
                else if (field[7][6] == 0) field[7][6] = p;
                else if (field[7][7] == 0) field[7][7] = p;
            }
            //endregion
            //region Field 10x10 Random
            if(field.length==10) {
                if (field[k][r] == 0)
                    field[k][r] = p;
                else if (field[0][0] == 0) field[0][0] = p;
                else if (field[0][1] == 0) field[0][1] = p;
                else if (field[0][2] == 0) field[0][2] = p;
                else if (field[0][3] == 0) field[0][3] = p;
                else if (field[0][4] == 0) field[0][4] = p;
                else if (field[0][5] == 0) field[0][5] = p;
                else if (field[0][6] == 0) field[0][6] = p;
                else if (field[0][7] == 0) field[0][7] = p;
                else if (field[0][8] == 0) field[0][8] = p;
                else if (field[0][9] == 0) field[0][9] = p;


                else if (field[1][0] == 0) field[1][0] = p;
                else if (field[1][1] == 0) field[1][1] = p;
                else if (field[1][2] == 0) field[1][2] = p;
                else if (field[1][3] == 0) field[1][3] = p;
                else if (field[1][4] == 0) field[1][4] = p;
                else if (field[1][5] == 0) field[1][5] = p;
                else if (field[1][6] == 0) field[1][6] = p;
                else if (field[1][7] == 0) field[1][7] = p;
                else if (field[1][8] == 0) field[1][8] = p;
                else if (field[1][9] == 0) field[1][9] = p;


                else if (field[2][0] == 0) field[2][0] = p;
                else if (field[2][1] == 0) field[2][1] = p;
                else if (field[2][2] == 0) field[2][2] = p;
                else if (field[2][3] == 0) field[2][3] = p;
                else if (field[2][4] == 0) field[2][4] = p;
                else if (field[2][5] == 0) field[2][5] = p;
                else if (field[2][6] == 0) field[2][6] = p;
                else if (field[2][7] == 0) field[2][7] = p;
                else if (field[2][8] == 0) field[2][8] = p;
                else if (field[2][9] == 0) field[2][9] = p;


                else if (field[3][0] == 0) field[3][0] = p;
                else if (field[3][1] == 0) field[3][1] = p;
                else if (field[3][2] == 0) field[3][2] = p;
                else if (field[3][3] == 0) field[3][3] = p;
                else if (field[3][4] == 0) field[3][4] = p;
                else if (field[3][5] == 0) field[3][5] = p;
                else if (field[3][6] == 0) field[3][6] = p;
                else if (field[3][7] == 0) field[3][7] = p;
                else if (field[3][8] == 0) field[3][8] = p;
                else if (field[3][9] == 0) field[3][9] = p;


                else if (field[4][0] == 0) field[4][0] = p;
                else if (field[4][1] == 0) field[4][1] = p;
                else if (field[4][2] == 0) field[4][2] = p;
                else if (field[4][3] == 0) field[4][3] = p;
                else if (field[4][4] == 0) field[4][4] = p;
                else if (field[4][5] == 0) field[4][5] = p;
                else if (field[4][6] == 0) field[4][6] = p;
                else if (field[4][7] == 0) field[4][7] = p;
                else if (field[4][8] == 0) field[4][8] = p;
                else if (field[4][9] == 0) field[4][9] = p;


                else if (field[5][0] == 0) field[5][0] = p;
                else if (field[5][1] == 0) field[5][1] = p;
                else if (field[5][2] == 0) field[5][2] = p;
                else if (field[5][3] == 0) field[5][3] = p;
                else if (field[5][4] == 0) field[5][4] = p;
                else if (field[5][5] == 0) field[5][5] = p;
                else if (field[5][6] == 0) field[5][6] = p;
                else if (field[5][7] == 0) field[5][7] = p;
                else if (field[5][8] == 0) field[5][8] = p;
                else if (field[5][9] == 0) field[5][9] = p;


                else if (field[6][0] == 0) field[6][0] = p;
                else if (field[6][1] == 0) field[6][1] = p;
                else if (field[6][2] == 0) field[6][2] = p;
                else if (field[6][3] == 0) field[6][3] = p;
                else if (field[6][4] == 0) field[6][4] = p;
                else if (field[6][5] == 0) field[6][5] = p;
                else if (field[6][6] == 0) field[6][6] = p;
                else if (field[6][7] == 0) field[6][7] = p;
                else if (field[6][8] == 0) field[6][8] = p;
                else if (field[6][9] == 0) field[6][9] = p;


                else if (field[7][0] == 0) field[7][0] = p;
                else if (field[7][1] == 0) field[7][1] = p;
                else if (field[7][2] == 0) field[7][2] = p;
                else if (field[7][3] == 0) field[7][3] = p;
                else if (field[7][4] == 0) field[7][4] = p;
                else if (field[7][5] == 0) field[7][5] = p;
                else if (field[7][6] == 0) field[7][6] = p;
                else if (field[7][7] == 0) field[7][7] = p;
                else if (field[7][8] == 0) field[7][8] = p;
                else if (field[7][9] == 0) field[7][9] = p;


                else if (field[8][0] == 0) field[8][0] = p;
                else if (field[8][1] == 0) field[8][1] = p;
                else if (field[8][2] == 0) field[8][2] = p;
                else if (field[8][3] == 0) field[8][3] = p;
                else if (field[8][4] == 0) field[8][4] = p;
                else if (field[8][5] == 0) field[8][5] = p;
                else if (field[8][6] == 0) field[8][6] = p;
                else if (field[8][7] == 0) field[8][7] = p;
                else if (field[8][8] == 0) field[8][8] = p;
                else if (field[8][9] == 0) field[8][9] = p;


                else if (field[9][0] == 0) field[9][0] = p;
                else if (field[9][1] == 0) field[9][1] = p;
                else if (field[9][2] == 0) field[9][2] = p;
                else if (field[9][3] == 0) field[9][3] = p;
                else if (field[9][4] == 0) field[9][4] = p;
                else if (field[9][5] == 0) field[9][5] = p;
                else if (field[9][6] == 0) field[9][6] = p;
                else if (field[9][7] == 0) field[9][7] = p;
                else if (field[9][8] == 0) field[9][8] = p;
                else if (field[9][9] == 0) field[9][9] = p;
            }
            //endregion


            System.out.println(summ);
        } else {
            System.out.println("Erroe");
            try {

                gameOver();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        YetiGame.getInstance().setScore(summ);
    }

    public void moveDown() {
        if (canMove() == true) {
            summ += 2;
            System.out.println("down");

           /* for (int i = 0; i < field.length; i++) {
                for (int j = 0; j < field[i].length; j++) {
                    //i - строка
                    //j - столбец
                    int pv = 0;
                    int row = 1;
                    while (row < field[i].length) {
                        if (field[j][row] == 0)
                            row++;


                        else if (field[j][pv] == 0) {
                            field[j][pv] = field[j][row];
                            field[j][row++] = 0;

                        } else if (field[j][pv] == field[j][row]) {
                            field[j][pv++] += field[j][row];
                            field[j][row++] = 0;
                        } else if (++pv == row)
                            row++;
                   }}}*/
            for (int col = 0; col < field.length; col++)
            {

                int pivot = 0, row = 1;

                while (row < field.length)
                {

                    if (field[col][row] == 0)
                        row++;


                    else if (field[col][pivot] == 0) {
                        field[col][pivot] = field[col][row];
                        field[col][row++] = 0;

                    } else if (field[col][pivot] == field[col][row]) {
                        field[col][pivot++] += field[col][row];
                        field[col][row++] = 0;
                    } else if (++pivot == row)
                        row++;
                }
            }



            //region Field 4x4 Random
            Random rnd = new Random();
            int p = rnd.nextInt(5);
            int k = rnd.nextInt(4);
            int r = rnd.nextInt(4);
            if (p == 0 || p == 1 || p == 3) {
                p = 2;

            }

            if(field.length==4) {
                if (field[k][r] == 0) field[k][r] = p;
                else if (field[0][0] == 0) field[0][0] = p;
                else if (field[0][1] == 0) field[0][1] = p;
                else if (field[0][2] == 0) field[0][2] = p;
                else if (field[0][3] == 0) field[0][3] = p;
                else if (field[1][0] == 0) field[1][0] = p;
                else if (field[1][1] == 0) field[1][1] = p;
                else if (field[1][2] == 0) field[1][2] = p;
                else if (field[1][3] == 0) field[1][3] = p;
                else if (field[2][0] == 0) field[2][0] = p;
                else if (field[2][1] == 0) field[2][1] = p;
                else if (field[2][2] == 0) field[2][2] = p;
                else if (field[2][3] == 0) field[2][3] = p;
                else if (field[3][0] == 0) field[3][0] = p;
                else if (field[3][1] == 0) field[3][1] = p;
                else if (field[3][2] == 0) field[3][2] = p;
                else if (field[3][3] == 0) field[3][3] = p;
            }
            //endregion
            //region Field 6x6 Random
            if(field.length==6) {
                if (field[k][r] == 0)
                    field[k][r] = p;
                else if (field[0][0] == 0) field[0][0] = p;
                else if (field[0][1] == 0) field[0][1] = p;
                else if (field[0][2] == 0) field[0][2] = p;
                else if (field[0][3] == 0) field[0][3] = p;
                else if (field[0][4] == 0) field[0][4] = p;
                else if (field[0][5] == 0) field[0][5] = p;


                else if (field[1][0] == 0) field[1][0] = p;
                else if (field[1][1] == 0) field[1][1] = p;
                else if (field[1][2] == 0) field[1][2] = p;
                else if (field[1][3] == 0) field[1][3] = p;
                else if (field[1][4] == 0) field[1][4] = p;
                else if (field[1][5] == 0) field[1][5] = p;


                else if (field[2][0] == 0) field[2][0] = p;
                else if (field[2][1] == 0) field[2][1] = p;
                else if (field[2][2] == 0) field[2][2] = p;
                else if (field[2][3] == 0) field[2][3] = p;
                else if (field[2][4] == 0) field[2][4] = p;
                else if (field[2][5] == 0) field[2][5] = p;



                else if (field[3][0] == 0) field[3][0] = p;
                else if (field[3][1] == 0) field[3][1] = p;
                else if (field[3][2] == 0) field[3][2] = p;
                else if (field[3][3] == 0) field[3][3] = p;
                else if (field[3][4] == 0) field[3][4] = p;
                else if (field[3][5] == 0) field[3][5] = p;


                else if (field[4][0] == 0) field[4][0] = p;
                else if (field[4][1] == 0) field[4][1] = p;
                else if (field[4][2] == 0) field[4][2] = p;
                else if (field[4][3] == 0) field[4][3] = p;
                else if (field[4][4] == 0) field[4][4] = p;
                else if (field[4][5] == 0) field[4][5] = p;


                else if (field[5][0] == 0) field[5][0] = p;
                else if (field[5][1] == 0) field[5][1] = p;
                else if (field[5][2] == 0) field[5][2] = p;
                else if (field[5][3] == 0) field[5][3] = p;
                else if (field[5][4] == 0) field[5][4] = p;
                else if (field[5][5] == 0) field[5][5] = p;


            }
            //endregion
            //region Field 8x8 Random
            if(field.length==8) {
                if (field[k][r] == 0)
                    field[k][r] = p;
                else if (field[0][0] == 0) field[0][0] = p;
                else if (field[0][1] == 0) field[0][1] = p;
                else if (field[0][2] == 0) field[0][2] = p;
                else if (field[0][3] == 0) field[0][3] = p;
                else if (field[0][4] == 0) field[0][4] = p;
                else if (field[0][5] == 0) field[0][5] = p;
                else if (field[0][6] == 0) field[0][6] = p;
                else if (field[0][7] == 0) field[0][7] = p;


                else if (field[1][0] == 0) field[1][0] = p;
                else if (field[1][1] == 0) field[1][1] = p;
                else if (field[1][2] == 0) field[1][2] = p;
                else if (field[1][3] == 0) field[1][3] = p;
                else if (field[1][4] == 0) field[1][4] = p;
                else if (field[1][5] == 0) field[1][5] = p;
                else if (field[1][6] == 0) field[1][6] = p;
                else if (field[1][7] == 0) field[1][7] = p;


                else if (field[2][0] == 0) field[2][0] = p;
                else if (field[2][1] == 0) field[2][1] = p;
                else if (field[2][2] == 0) field[2][2] = p;
                else if (field[2][3] == 0) field[2][3] = p;
                else if (field[2][4] == 0) field[2][4] = p;
                else if (field[2][5] == 0) field[2][5] = p;
                else if (field[2][6] == 0) field[2][6] = p;
                else if (field[2][7] == 0) field[2][7] = p;


                else if (field[3][0] == 0) field[3][0] = p;
                else if (field[3][1] == 0) field[3][1] = p;
                else if (field[3][2] == 0) field[3][2] = p;
                else if (field[3][3] == 0) field[3][3] = p;
                else if (field[3][4] == 0) field[3][4] = p;
                else if (field[3][5] == 0) field[3][5] = p;
                else if (field[3][6] == 0) field[3][6] = p;
                else if (field[3][7] == 0) field[3][7] = p;


                else if (field[4][0] == 0) field[4][0] = p;
                else if (field[4][1] == 0) field[4][1] = p;
                else if (field[4][2] == 0) field[4][2] = p;
                else if (field[4][3] == 0) field[4][3] = p;
                else if (field[4][4] == 0) field[4][4] = p;
                else if (field[4][5] == 0) field[4][5] = p;
                else if (field[4][6] == 0) field[4][6] = p;
                else if (field[4][7] == 0) field[4][7] = p;


                else if (field[5][0] == 0) field[5][0] = p;
                else if (field[5][1] == 0) field[5][1] = p;
                else if (field[5][2] == 0) field[5][2] = p;
                else if (field[5][3] == 0) field[5][3] = p;
                else if (field[5][4] == 0) field[5][4] = p;
                else if (field[5][5] == 0) field[5][5] = p;
                else if (field[5][6] == 0) field[5][6] = p;
                else if (field[5][7] == 0) field[5][7] = p;


                else if (field[6][0] == 0) field[6][0] = p;
                else if (field[6][1] == 0) field[6][1] = p;
                else if (field[6][2] == 0) field[6][2] = p;
                else if (field[6][3] == 0) field[6][3] = p;
                else if (field[6][4] == 0) field[6][4] = p;
                else if (field[6][5] == 0) field[6][5] = p;
                else if (field[6][6] == 0) field[6][6] = p;
                else if (field[6][7] == 0) field[6][7] = p;


                else if (field[7][0] == 0) field[7][0] = p;
                else if (field[7][1] == 0) field[7][1] = p;
                else if (field[7][2] == 0) field[7][2] = p;
                else if (field[7][3] == 0) field[7][3] = p;
                else if (field[7][4] == 0) field[7][4] = p;
                else if (field[7][5] == 0) field[7][5] = p;
                else if (field[7][6] == 0) field[7][6] = p;
                else if (field[7][7] == 0) field[7][7] = p;
            }
            //endregion
            //region Field 10x10 Random
            if(field.length==10) {
                if (field[k][r] == 0)
                    field[k][r] = p;
                else if (field[0][0] == 0) field[0][0] = p;
                else if (field[0][1] == 0) field[0][1] = p;
                else if (field[0][2] == 0) field[0][2] = p;
                else if (field[0][3] == 0) field[0][3] = p;
                else if (field[0][4] == 0) field[0][4] = p;
                else if (field[0][5] == 0) field[0][5] = p;
                else if (field[0][6] == 0) field[0][6] = p;
                else if (field[0][7] == 0) field[0][7] = p;
                else if (field[0][8] == 0) field[0][8] = p;
                else if (field[0][9] == 0) field[0][9] = p;


                else if (field[1][0] == 0) field[1][0] = p;
                else if (field[1][1] == 0) field[1][1] = p;
                else if (field[1][2] == 0) field[1][2] = p;
                else if (field[1][3] == 0) field[1][3] = p;
                else if (field[1][4] == 0) field[1][4] = p;
                else if (field[1][5] == 0) field[1][5] = p;
                else if (field[1][6] == 0) field[1][6] = p;
                else if (field[1][7] == 0) field[1][7] = p;
                else if (field[1][8] == 0) field[1][8] = p;
                else if (field[1][9] == 0) field[1][9] = p;


                else if (field[2][0] == 0) field[2][0] = p;
                else if (field[2][1] == 0) field[2][1] = p;
                else if (field[2][2] == 0) field[2][2] = p;
                else if (field[2][3] == 0) field[2][3] = p;
                else if (field[2][4] == 0) field[2][4] = p;
                else if (field[2][5] == 0) field[2][5] = p;
                else if (field[2][6] == 0) field[2][6] = p;
                else if (field[2][7] == 0) field[2][7] = p;
                else if (field[2][8] == 0) field[2][8] = p;
                else if (field[2][9] == 0) field[2][9] = p;


                else if (field[3][0] == 0) field[3][0] = p;
                else if (field[3][1] == 0) field[3][1] = p;
                else if (field[3][2] == 0) field[3][2] = p;
                else if (field[3][3] == 0) field[3][3] = p;
                else if (field[3][4] == 0) field[3][4] = p;
                else if (field[3][5] == 0) field[3][5] = p;
                else if (field[3][6] == 0) field[3][6] = p;
                else if (field[3][7] == 0) field[3][7] = p;
                else if (field[3][8] == 0) field[3][8] = p;
                else if (field[3][9] == 0) field[3][9] = p;


                else if (field[4][0] == 0) field[4][0] = p;
                else if (field[4][1] == 0) field[4][1] = p;
                else if (field[4][2] == 0) field[4][2] = p;
                else if (field[4][3] == 0) field[4][3] = p;
                else if (field[4][4] == 0) field[4][4] = p;
                else if (field[4][5] == 0) field[4][5] = p;
                else if (field[4][6] == 0) field[4][6] = p;
                else if (field[4][7] == 0) field[4][7] = p;
                else if (field[4][8] == 0) field[4][8] = p;
                else if (field[4][9] == 0) field[4][9] = p;


                else if (field[5][0] == 0) field[5][0] = p;
                else if (field[5][1] == 0) field[5][1] = p;
                else if (field[5][2] == 0) field[5][2] = p;
                else if (field[5][3] == 0) field[5][3] = p;
                else if (field[5][4] == 0) field[5][4] = p;
                else if (field[5][5] == 0) field[5][5] = p;
                else if (field[5][6] == 0) field[5][6] = p;
                else if (field[5][7] == 0) field[5][7] = p;
                else if (field[5][8] == 0) field[5][8] = p;
                else if (field[5][9] == 0) field[5][9] = p;


                else if (field[6][0] == 0) field[6][0] = p;
                else if (field[6][1] == 0) field[6][1] = p;
                else if (field[6][2] == 0) field[6][2] = p;
                else if (field[6][3] == 0) field[6][3] = p;
                else if (field[6][4] == 0) field[6][4] = p;
                else if (field[6][5] == 0) field[6][5] = p;
                else if (field[6][6] == 0) field[6][6] = p;
                else if (field[6][7] == 0) field[6][7] = p;
                else if (field[6][8] == 0) field[6][8] = p;
                else if (field[6][9] == 0) field[6][9] = p;


                else if (field[7][0] == 0) field[7][0] = p;
                else if (field[7][1] == 0) field[7][1] = p;
                else if (field[7][2] == 0) field[7][2] = p;
                else if (field[7][3] == 0) field[7][3] = p;
                else if (field[7][4] == 0) field[7][4] = p;
                else if (field[7][5] == 0) field[7][5] = p;
                else if (field[7][6] == 0) field[7][6] = p;
                else if (field[7][7] == 0) field[7][7] = p;
                else if (field[7][8] == 0) field[7][8] = p;
                else if (field[7][9] == 0) field[7][9] = p;


                else if (field[8][0] == 0) field[8][0] = p;
                else if (field[8][1] == 0) field[8][1] = p;
                else if (field[8][2] == 0) field[8][2] = p;
                else if (field[8][3] == 0) field[8][3] = p;
                else if (field[8][4] == 0) field[8][4] = p;
                else if (field[8][5] == 0) field[8][5] = p;
                else if (field[8][6] == 0) field[8][6] = p;
                else if (field[8][7] == 0) field[8][7] = p;
                else if (field[8][8] == 0) field[8][8] = p;
                else if (field[8][9] == 0) field[8][9] = p;


                else if (field[9][0] == 0) field[9][0] = p;
                else if (field[9][1] == 0) field[9][1] = p;
                else if (field[9][2] == 0) field[9][2] = p;
                else if (field[9][3] == 0) field[9][3] = p;
                else if (field[9][4] == 0) field[9][4] = p;
                else if (field[9][5] == 0) field[9][5] = p;
                else if (field[9][6] == 0) field[9][6] = p;
                else if (field[9][7] == 0) field[9][7] = p;
                else if (field[9][8] == 0) field[9][8] = p;
                else if (field[9][9] == 0) field[9][9] = p;
            }
            //endregion

            System.out.println(summ);
        } else {
            System.out.println("Erroe");
            try {

                gameOver();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        YetiGame.getInstance().setScore(summ);
    }

    public void moveUp() {
        if (canMove() == true) {
            System.out.println("up");

            summ += 2;
            /*for (int i = 0; i < field.length; i++) {
                for (int j = 0; j < field[i].length; j++) {
                    //i - строка
                    //j - столбец
                    int pv = field.length - 1;
                    int row = field.length - 2;

                    while (row >= 0) {
                        if (field[j][row] == 0) {
                            row--;

                        } else if (field[j][pv] == 0) {
                            field[j][pv] = field[j][row];
                            field[j][row--] = 0;

                        } else if (field[j][pv] == field[j][row]) {
                            field[j][pv--] += field[j][row];
                            field[j][row--] = 0;

                        } else if (--pv == row)
                            row--;
                 } }}*/

            for (int col = 0; col < field.length; col++)
            {

                int pivot = field.length - 1;
                int row = field.length - 2;
                while (row >= 0) {


                    if (field[col][row] == 0) {
                        row--;

                    } else if (field[col][pivot] == 0) {
                        field[col][pivot] = field[col][row];
                        field[col][row--] = 0;

                    } else if (field[col][pivot] == field[col][row]) {
                        field[col][pivot--] += field[col][row];
                        field[col][row--] = 0;

                    } else if (--pivot == row)
                        row--;
                }
            }

              //region Field 4x4 Random
            Random rnd = new Random();
            int p = rnd.nextInt(5);
            int k = rnd.nextInt(4);
            int r = rnd.nextInt(4);
            if (p == 0 || p == 1 || p == 3) {
                p = 2;

            }

            if(field.length==4) {
                if (field[k][r] == 0) field[k][r] = p;
                else if (field[0][0] == 0) field[0][0] = p;
                else if (field[0][1] == 0) field[0][1] = p;
                else if (field[0][2] == 0) field[0][2] = p;
                else if (field[0][3] == 0) field[0][3] = p;
                else if (field[1][0] == 0) field[1][0] = p;
                else if (field[1][1] == 0) field[1][1] = p;
                else if (field[1][2] == 0) field[1][2] = p;
                else if (field[1][3] == 0) field[1][3] = p;
                else if (field[2][0] == 0) field[2][0] = p;
                else if (field[2][1] == 0) field[2][1] = p;
                else if (field[2][2] == 0) field[2][2] = p;
                else if (field[2][3] == 0) field[2][3] = p;
                else if (field[3][0] == 0) field[3][0] = p;
                else if (field[3][1] == 0) field[3][1] = p;
                else if (field[3][2] == 0) field[3][2] = p;
                else if (field[3][3] == 0) field[3][3] = p;
            }
            //endregion
            //region Field 6x6 Random
            if(field.length==6) {
                if (field[k][r] == 0)
                    field[k][r] = p;
                else if (field[0][0] == 0) field[0][0] = p;
                else if (field[0][1] == 0) field[0][1] = p;
                else if (field[0][2] == 0) field[0][2] = p;
                else if (field[0][3] == 0) field[0][3] = p;
                else if (field[0][4] == 0) field[0][4] = p;
                else if (field[0][5] == 0) field[0][5] = p;


                else if (field[1][0] == 0) field[1][0] = p;
                else if (field[1][1] == 0) field[1][1] = p;
                else if (field[1][2] == 0) field[1][2] = p;
                else if (field[1][3] == 0) field[1][3] = p;
                else if (field[1][4] == 0) field[1][4] = p;
                else if (field[1][5] == 0) field[1][5] = p;


                else if (field[2][0] == 0) field[2][0] = p;
                else if (field[2][1] == 0) field[2][1] = p;
                else if (field[2][2] == 0) field[2][2] = p;
                else if (field[2][3] == 0) field[2][3] = p;
                else if (field[2][4] == 0) field[2][4] = p;
                else if (field[2][5] == 0) field[2][5] = p;



                else if (field[3][0] == 0) field[3][0] = p;
                else if (field[3][1] == 0) field[3][1] = p;
                else if (field[3][2] == 0) field[3][2] = p;
                else if (field[3][3] == 0) field[3][3] = p;
                else if (field[3][4] == 0) field[3][4] = p;
                else if (field[3][5] == 0) field[3][5] = p;


                else if (field[4][0] == 0) field[4][0] = p;
                else if (field[4][1] == 0) field[4][1] = p;
                else if (field[4][2] == 0) field[4][2] = p;
                else if (field[4][3] == 0) field[4][3] = p;
                else if (field[4][4] == 0) field[4][4] = p;
                else if (field[4][5] == 0) field[4][5] = p;


                else if (field[5][0] == 0) field[5][0] = p;
                else if (field[5][1] == 0) field[5][1] = p;
                else if (field[5][2] == 0) field[5][2] = p;
                else if (field[5][3] == 0) field[5][3] = p;
                else if (field[5][4] == 0) field[5][4] = p;
                else if (field[5][5] == 0) field[5][5] = p;


            }
            //endregion
            //region Field 8x8 Random
            if(field.length==8) {
                if (field[k][r] == 0)
                    field[k][r] = p;
                else if (field[0][0] == 0) field[0][0] = p;
                else if (field[0][1] == 0) field[0][1] = p;
                else if (field[0][2] == 0) field[0][2] = p;
                else if (field[0][3] == 0) field[0][3] = p;
                else if (field[0][4] == 0) field[0][4] = p;
                else if (field[0][5] == 0) field[0][5] = p;
                else if (field[0][6] == 0) field[0][6] = p;
                else if (field[0][7] == 0) field[0][7] = p;


                else if (field[1][0] == 0) field[1][0] = p;
                else if (field[1][1] == 0) field[1][1] = p;
                else if (field[1][2] == 0) field[1][2] = p;
                else if (field[1][3] == 0) field[1][3] = p;
                else if (field[1][4] == 0) field[1][4] = p;
                else if (field[1][5] == 0) field[1][5] = p;
                else if (field[1][6] == 0) field[1][6] = p;
                else if (field[1][7] == 0) field[1][7] = p;


                else if (field[2][0] == 0) field[2][0] = p;
                else if (field[2][1] == 0) field[2][1] = p;
                else if (field[2][2] == 0) field[2][2] = p;
                else if (field[2][3] == 0) field[2][3] = p;
                else if (field[2][4] == 0) field[2][4] = p;
                else if (field[2][5] == 0) field[2][5] = p;
                else if (field[2][6] == 0) field[2][6] = p;
                else if (field[2][7] == 0) field[2][7] = p;


                else if (field[3][0] == 0) field[3][0] = p;
                else if (field[3][1] == 0) field[3][1] = p;
                else if (field[3][2] == 0) field[3][2] = p;
                else if (field[3][3] == 0) field[3][3] = p;
                else if (field[3][4] == 0) field[3][4] = p;
                else if (field[3][5] == 0) field[3][5] = p;
                else if (field[3][6] == 0) field[3][6] = p;
                else if (field[3][7] == 0) field[3][7] = p;


                else if (field[4][0] == 0) field[4][0] = p;
                else if (field[4][1] == 0) field[4][1] = p;
                else if (field[4][2] == 0) field[4][2] = p;
                else if (field[4][3] == 0) field[4][3] = p;
                else if (field[4][4] == 0) field[4][4] = p;
                else if (field[4][5] == 0) field[4][5] = p;
                else if (field[4][6] == 0) field[4][6] = p;
                else if (field[4][7] == 0) field[4][7] = p;


                else if (field[5][0] == 0) field[5][0] = p;
                else if (field[5][1] == 0) field[5][1] = p;
                else if (field[5][2] == 0) field[5][2] = p;
                else if (field[5][3] == 0) field[5][3] = p;
                else if (field[5][4] == 0) field[5][4] = p;
                else if (field[5][5] == 0) field[5][5] = p;
                else if (field[5][6] == 0) field[5][6] = p;
                else if (field[5][7] == 0) field[5][7] = p;


                else if (field[6][0] == 0) field[6][0] = p;
                else if (field[6][1] == 0) field[6][1] = p;
                else if (field[6][2] == 0) field[6][2] = p;
                else if (field[6][3] == 0) field[6][3] = p;
                else if (field[6][4] == 0) field[6][4] = p;
                else if (field[6][5] == 0) field[6][5] = p;
                else if (field[6][6] == 0) field[6][6] = p;
                else if (field[6][7] == 0) field[6][7] = p;


                else if (field[7][0] == 0) field[7][0] = p;
                else if (field[7][1] == 0) field[7][1] = p;
                else if (field[7][2] == 0) field[7][2] = p;
                else if (field[7][3] == 0) field[7][3] = p;
                else if (field[7][4] == 0) field[7][4] = p;
                else if (field[7][5] == 0) field[7][5] = p;
                else if (field[7][6] == 0) field[7][6] = p;
                else if (field[7][7] == 0) field[7][7] = p;
            }
            //endregion
            //region Field 10x10 Random
            if(field.length==10) {
                if (field[k][r] == 0)
                    field[k][r] = p;
                else if (field[0][0] == 0) field[0][0] = p;
                else if (field[0][1] == 0) field[0][1] = p;
                else if (field[0][2] == 0) field[0][2] = p;
                else if (field[0][3] == 0) field[0][3] = p;
                else if (field[0][4] == 0) field[0][4] = p;
                else if (field[0][5] == 0) field[0][5] = p;
                else if (field[0][6] == 0) field[0][6] = p;
                else if (field[0][7] == 0) field[0][7] = p;
                else if (field[0][8] == 0) field[0][8] = p;
                else if (field[0][9] == 0) field[0][9] = p;


                else if (field[1][0] == 0) field[1][0] = p;
                else if (field[1][1] == 0) field[1][1] = p;
                else if (field[1][2] == 0) field[1][2] = p;
                else if (field[1][3] == 0) field[1][3] = p;
                else if (field[1][4] == 0) field[1][4] = p;
                else if (field[1][5] == 0) field[1][5] = p;
                else if (field[1][6] == 0) field[1][6] = p;
                else if (field[1][7] == 0) field[1][7] = p;
                else if (field[1][8] == 0) field[1][8] = p;
                else if (field[1][9] == 0) field[1][9] = p;


                else if (field[2][0] == 0) field[2][0] = p;
                else if (field[2][1] == 0) field[2][1] = p;
                else if (field[2][2] == 0) field[2][2] = p;
                else if (field[2][3] == 0) field[2][3] = p;
                else if (field[2][4] == 0) field[2][4] = p;
                else if (field[2][5] == 0) field[2][5] = p;
                else if (field[2][6] == 0) field[2][6] = p;
                else if (field[2][7] == 0) field[2][7] = p;
                else if (field[2][8] == 0) field[2][8] = p;
                else if (field[2][9] == 0) field[2][9] = p;


                else if (field[3][0] == 0) field[3][0] = p;
                else if (field[3][1] == 0) field[3][1] = p;
                else if (field[3][2] == 0) field[3][2] = p;
                else if (field[3][3] == 0) field[3][3] = p;
                else if (field[3][4] == 0) field[3][4] = p;
                else if (field[3][5] == 0) field[3][5] = p;
                else if (field[3][6] == 0) field[3][6] = p;
                else if (field[3][7] == 0) field[3][7] = p;
                else if (field[3][8] == 0) field[3][8] = p;
                else if (field[3][9] == 0) field[3][9] = p;


                else if (field[4][0] == 0) field[4][0] = p;
                else if (field[4][1] == 0) field[4][1] = p;
                else if (field[4][2] == 0) field[4][2] = p;
                else if (field[4][3] == 0) field[4][3] = p;
                else if (field[4][4] == 0) field[4][4] = p;
                else if (field[4][5] == 0) field[4][5] = p;
                else if (field[4][6] == 0) field[4][6] = p;
                else if (field[4][7] == 0) field[4][7] = p;
                else if (field[4][8] == 0) field[4][8] = p;
                else if (field[4][9] == 0) field[4][9] = p;


                else if (field[5][0] == 0) field[5][0] = p;
                else if (field[5][1] == 0) field[5][1] = p;
                else if (field[5][2] == 0) field[5][2] = p;
                else if (field[5][3] == 0) field[5][3] = p;
                else if (field[5][4] == 0) field[5][4] = p;
                else if (field[5][5] == 0) field[5][5] = p;
                else if (field[5][6] == 0) field[5][6] = p;
                else if (field[5][7] == 0) field[5][7] = p;
                else if (field[5][8] == 0) field[5][8] = p;
                else if (field[5][9] == 0) field[5][9] = p;


                else if (field[6][0] == 0) field[6][0] = p;
                else if (field[6][1] == 0) field[6][1] = p;
                else if (field[6][2] == 0) field[6][2] = p;
                else if (field[6][3] == 0) field[6][3] = p;
                else if (field[6][4] == 0) field[6][4] = p;
                else if (field[6][5] == 0) field[6][5] = p;
                else if (field[6][6] == 0) field[6][6] = p;
                else if (field[6][7] == 0) field[6][7] = p;
                else if (field[6][8] == 0) field[6][8] = p;
                else if (field[6][9] == 0) field[6][9] = p;


                else if (field[7][0] == 0) field[7][0] = p;
                else if (field[7][1] == 0) field[7][1] = p;
                else if (field[7][2] == 0) field[7][2] = p;
                else if (field[7][3] == 0) field[7][3] = p;
                else if (field[7][4] == 0) field[7][4] = p;
                else if (field[7][5] == 0) field[7][5] = p;
                else if (field[7][6] == 0) field[7][6] = p;
                else if (field[7][7] == 0) field[7][7] = p;
                else if (field[7][8] == 0) field[7][8] = p;
                else if (field[7][9] == 0) field[7][9] = p;


                else if (field[8][0] == 0) field[8][0] = p;
                else if (field[8][1] == 0) field[8][1] = p;
                else if (field[8][2] == 0) field[8][2] = p;
                else if (field[8][3] == 0) field[8][3] = p;
                else if (field[8][4] == 0) field[8][4] = p;
                else if (field[8][5] == 0) field[8][5] = p;
                else if (field[8][6] == 0) field[8][6] = p;
                else if (field[8][7] == 0) field[8][7] = p;
                else if (field[8][8] == 0) field[8][8] = p;
                else if (field[8][9] == 0) field[8][9] = p;


                else if (field[9][0] == 0) field[9][0] = p;
                else if (field[9][1] == 0) field[9][1] = p;
                else if (field[9][2] == 0) field[9][2] = p;
                else if (field[9][3] == 0) field[9][3] = p;
                else if (field[9][4] == 0) field[9][4] = p;
                else if (field[9][5] == 0) field[9][5] = p;
                else if (field[9][6] == 0) field[9][6] = p;
                else if (field[9][7] == 0) field[9][7] = p;
                else if (field[9][8] == 0) field[9][8] = p;
                else if (field[9][9] == 0) field[9][9] = p;
            }
            //endregion

            System.out.println(summ);
        } else {
            System.out.println("Erroe");

            try {

                gameOver();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        YetiGame.getInstance().setScore(summ);

    }
    public void gameOver() throws IOException {
        YetiGame.getInstance().setHighScore();
        YetiGame.getInstance().moveToGameOver();

    }
}
