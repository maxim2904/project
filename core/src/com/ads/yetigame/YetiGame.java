package com.ads.yetigame;


import com.ads.yetigame.Screens.GameMenuScreen;
import com.ads.yetigame.Screens.MainGameScreen;
import com.ads.yetigame.Screens.championsScreen;
import com.ads.yetigame.Screens.gameOverScreen;
import com.ads.yetigame.Screens.infoscreen;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class YetiGame extends Game {

    public static final float WORLD_WIDTH = 640f;
    public static final float WORLD_HEIGHT = 1136f;
    private static YetiGame instance = new YetiGame();

    public static YetiGame getInstance() {
        return instance;
    }

    private YetiGame() {

    }
    private float ppuX;
    private float ppuY;
    private SpriteBatch batch;
    private ShapeRenderer shape;
    public BitmapFont font;
    private static final String FONT_CHARACTERS = "абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789][_!$%#@|\\/?-+=()*&.;,{}\"´`'<>";
    public File file;
    public int HighScore;
    public int Score = 0;
    public MainGameScreen gameScreen;
    public gameOverScreen GameOverScreen;

    private GameMenuScreen gameMenuScreen;
    private championsScreen championsScreen;
    private infoscreen infoScreen;
    private MainGameScreen mainGaneScreen;

    @Override
    public void create() {

        ppuX = Gdx.graphics.getWidth() / WORLD_WIDTH;
        ppuY = Gdx.graphics.getHeight() / WORLD_HEIGHT;
        shape = new ShapeRenderer();

        batch = new SpriteBatch();
        font = new BitmapFont();
        gameMenuScreen = new GameMenuScreen(batch);
        championsScreen = new championsScreen(batch);
        GameOverScreen = new gameOverScreen(batch);
      //  gameScreen = new MainGameScreen(batch);
        //gameScreen = new MainGameScreen(batch);
      /*  try {
            gameScreen = new MainGameScreen(batch);
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        infoScreen = new infoscreen(batch);
        mainGaneScreen = new MainGameScreen(batch);

        moveToMainMenu();
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("android/assets/fonts/shrift.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter param = new FreeTypeFontGenerator.FreeTypeFontParameter();
        param.size = Gdx.graphics.getHeight() / 17; // Размер шрифта. Я сделал его исходя из размеров экрана. Правда коряво, но вы сами можете поиграться, как вам угодно.
        param.characters = FONT_CHARACTERS; // Наши символы
        font = generator.generateFont(param); // Генерируем шрифт
        param.size = Gdx.graphics.getHeight() / 20;
        // levels = generator.generateFont(param);
        font.setColor(255f / 255f, 255f / 255f, 218f / 255f, 1f); // Цвет белый
        //levels.setColor(Color.WHITE);
        generator.dispose();

        //com.badlogic.gdx.graphics.Color.GREEN
        //Gdx.graphics.getHeight()


    }

    public void moveToMainMenu() { //YetiGame.getInstance().moveToGameMenu();
        setScreen(gameMenuScreen);
    }

    public void moveToSettings() { //YetiGame.getInstance().moveToGameMenu();

        setScreen(championsScreen);
    }

    public ShapeRenderer getShapeRenderer() {
        return shape;
    }

    public BitmapFont getFont() {
        return font;
    }

    public float getPpuX() {
        return ppuX;
    }

    public void setPpuX(float ppuX) {
        this.ppuX = ppuX;
    }

    public float getPpuY() {
        return ppuY;
    }

    public void setPpuY(float ppuY) {
        this.ppuY = ppuY;
    }

    public void Exition() {
        System.exit(10);
    }

    public void MoveToMainGamescreen() {
        setScreen(mainGaneScreen);
    }

    public void moveToGameOver() {

        setScreen(GameOverScreen);
    }

    public void getHighScore() throws IOException {
        file = new File("android\\assets\\HighScore.txt");
        if (!file.exists())
            file.createNewFile();
        Scanner sc = new Scanner(file);
        String temp;
        temp = sc.nextLine();
        HighScore = Integer.parseInt(temp);
        sc.close();
    }

    public void setHighScore() throws IOException {
        int temp = YetiGame.getInstance().getScore();
        if (temp >= HighScore) {
            HighScore = temp;
            file = new File("android\\assets\\HighScore.txt");
            if (!file.exists())
                file.createNewFile();
            FileWriter out = new FileWriter(file, false);
            out.write(String.format(("%s"), temp));
            out.close();
        }
    }

    public int getScore() {
        return Score;
    }

    public void setScore(int score) {
        this.Score = score;
    }

    public void MoveToInfo() {
        setScreen(infoScreen);
    }
}
