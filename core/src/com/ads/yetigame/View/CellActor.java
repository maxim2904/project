package com.ads.yetigame.View;

import com.ads.yetigame.YetiGame;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by 04k1103 on 19.03.2016.
 */
public class CellActor extends Actor {
    private int num;
    private ShapeRenderer shape;
    private BitmapFont font;

    public CellActor(int num, float x, float y, float size) {
        setPosition(x + 3, y + 3);
        setSize(size - 6, size - 6);
        setNum(num);
        shape = YetiGame.getInstance().getShapeRenderer();
        font = YetiGame.getInstance().getFont();
    }

    public void draw(Batch batch, float parentAlpha) {
        batch.end();
        shape.setProjectionMatrix(batch.getProjectionMatrix());
        shape.begin(ShapeRenderer.ShapeType.Filled);
        shape.setColor(Color.BROWN);
        shape.rect(getX(), getY(), getWidth(), getHeight());
        shape.end();
        batch.begin();
        font.draw(batch, String.format("%d", num), getX(), getY());
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
