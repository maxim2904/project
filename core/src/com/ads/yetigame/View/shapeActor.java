package com.ads.yetigame.View;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by Максим on 02.03.2016.
 */
public class shapeActor extends Actor {
    ShapeRenderer shape;

    public shapeActor(float x, float y, float width, float height, ShapeRenderer shapeRenderer) {
        setPosition(x, y);
        setSize(width, height);
        shape = shapeRenderer;
    }

    public void draw(Batch batch, float parentAlpha) {
        batch.end();
        shape.setProjectionMatrix(batch.getProjectionMatrix());
        shape.begin(ShapeRenderer.ShapeType.Filled);

        shape.setColor(255f / 255f, 255f / 255f, 218f / 255f, 1f);
        shape.rect(getX(), getY(), getWidth(), getHeight());
        shape.end();
        batch.begin();
    }

}





