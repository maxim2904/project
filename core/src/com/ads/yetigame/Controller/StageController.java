package com.ads.yetigame.Controller;

import com.ads.yetigame.model.World;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;

/**
 * Created by 04k1103 on 26.03.2016.
 */
public class StageController implements InputProcessor {
    World world;

    public StageController(World world) {
        this.world = world;
    }

    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.LEFT:
                world.moveLeft();
                break;
            case Input.Keys.RIGHT:
                world.moveRight();
                break;
            case Input.Keys.UP:
                world.moveUp();
                break;
            case Input.Keys.DOWN:
                world.moveDown();
                break;

        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
