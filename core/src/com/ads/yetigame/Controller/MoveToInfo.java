package com.ads.yetigame.Controller;

import com.ads.yetigame.YetiGame;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Created by Максим on 01.02.2016.
 */
public class MoveToInfo extends ClickListener {
    public void clicked(InputEvent event, float x, float y) {
        YetiGame.getInstance().MoveToInfo();
    }
}
