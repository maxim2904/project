package com.ads.yetigame.Controller;

import com.ads.yetigame.YetiGame;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Created by Максим on 21.02.2016.
 */
public class MoveToMainGamescreen extends ClickListener {
    public void clicked(InputEvent event, float x, float y) {YetiGame.getInstance().MoveToMainGamescreen();
    }
}
