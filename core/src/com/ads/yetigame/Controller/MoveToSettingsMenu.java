package com.ads.yetigame.Controller;

import com.ads.yetigame.YetiGame;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;


public class MoveToSettingsMenu extends ClickListener {
    public void clicked(InputEvent event, float x, float y) {YetiGame.getInstance().moveToSettings();
    }
}