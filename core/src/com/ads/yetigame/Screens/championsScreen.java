package com.ads.yetigame.Screens;

import com.ads.yetigame.Controller.*;
import com.ads.yetigame.View.ImageActor;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;


public class championsScreen implements Screen {
    private Stage stage;
    private ImageActor backGround;
    private ImageActor backButton;

    public ScoreFontActor score;
    public ScoreFontActor highscore;
    public String resolvePath(String fileName) {
        return String.format(
                Gdx.app.getType() == Application.ApplicationType.Android ?
                        fileName :
                        String.format("android/assets/%s", fileName)
        );
    }

    public championsScreen(SpriteBatch batch) {
        score = new ScoreFontActor(true,160, 490, 72, Color.BLACK, "shrift.ttf");
        highscore = new ScoreFontActor(false,160, 490, 72, Color.WHITE
                ,"shrift.ttf");
        backGround = new ImageActor(new Texture(resolvePath("2048/bestscorebg.png")), 0, 0);
        backButton = new ImageActor(new Texture(resolvePath("2048/home.png")), 20, 20);
        backButton.addListener(new MoveToMainMenu());
        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);
        stage.addActor(backGround);
        stage.addActor(backButton);
        //stage.addActor(score);
        stage.addActor(highscore);

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }
}
