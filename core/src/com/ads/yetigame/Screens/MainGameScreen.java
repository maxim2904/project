package com.ads.yetigame.Screens;

import com.ads.yetigame.Controller.MoveToInfo;
import com.ads.yetigame.Controller.MoveToMainMenu;
import com.ads.yetigame.Controller.MoveToSettingsMenu;
import com.ads.yetigame.Controller.StageController;
import com.ads.yetigame.View.ImageActor;
import com.ads.yetigame.model.World;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.io.IOException;

/**
 * Created by Максим on 21.02.2016.
 */
public class MainGameScreen implements Screen {
    public World world;
    private Stage stage;
    private InputMultiplexer multiplexer;
    private ImageActor backGround;
    private ImageActor informationButton;

    private ImageActor championsButton;
    private ImageActor playButtonFour;
    private ImageActor playButtonSix;
    private ImageActor playButtonEight;
    private ImageActor playButtonTen;
    private ImageActor backButton;
    public String resolvePath(String fileName) {
        return String.format(
                Gdx.app.getType() == Application.ApplicationType.Android ?
                        fileName :
                        String.format("android/assets/%s", fileName)
        );
    }


    public MainGameScreen(SpriteBatch batch) {
        backGround = new ImageActor(new Texture(resolvePath("2048/bg-img3.png")), 0, 0);
        informationButton = new ImageActor(new Texture(resolvePath("2048/info-r-r.png")), 170, 10);
        informationButton.addListener(new MoveToInfo());
        championsButton = new ImageActor(new Texture(resolvePath("2048/champions.png")), 550, 900);
        championsButton.addListener(new MoveToSettingsMenu());
        backButton = new ImageActor(new Texture(resolvePath("2048/home.png")), 20, 20);
        backButton.addListener(new MoveToMainMenu());
        playButtonFour = new ImageActor(new Texture(resolvePath("2048/btn-four.png")), 80, 895);
        playButtonFour.addListener(new ClickListener() {
                    public void clicked(InputEvent event, float x, float y) {world = new World(4, new ScreenViewport(stage.getCamera()), stage.getBatch());refresh();}
                });
        playButtonSix = new ImageActor(new Texture(resolvePath("2048/btn-six.png")), 200, 895);
        playButtonSix.addListener(new ClickListener() {
                    public void clicked(InputEvent event, float x, float y) {world = new World(6, new ScreenViewport(stage.getCamera()), stage.getBatch());refresh();}
                });
        playButtonEight = new ImageActor(new Texture(resolvePath("2048/btn-eight.png")), 320, 895);
        playButtonEight.addListener(new ClickListener() {
                    public void clicked(InputEvent event, float x, float y) {world = new World(8, new ScreenViewport(stage.getCamera()), stage.getBatch());refresh();}});
        playButtonTen = new ImageActor(new Texture(resolvePath("2048/btn-ten.png")), 440, 895);
        playButtonTen.addListener(new ClickListener() {
                    public void clicked(InputEvent event, float x, float y) {world = new World(10, new ScreenViewport(stage.getCamera()), stage.getBatch());refresh();}
                });
        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);
        world = new World(1, new ScreenViewport(stage.getCamera()), stage.getBatch());
        stage.addActor(backGround);
        stage.addActor(informationButton);
        stage.addActor(championsButton);
        stage.addActor(playButtonFour);
        stage.addActor(playButtonSix);
        stage.addActor(playButtonEight);
        stage.addActor(playButtonTen);
        stage.addActor(backButton);
    }

    public void refresh() {
        multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(new StageController(world));
        multiplexer.addProcessor(stage);
        Gdx.input.setInputProcessor(multiplexer);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();

        if (world != null) world.draw();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void show() {

        refresh();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

}






