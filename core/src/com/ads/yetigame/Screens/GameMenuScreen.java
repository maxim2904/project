package com.ads.yetigame.Screens;

import com.ads.yetigame.Controller.*;
import com.ads.yetigame.View.ImageActor;
import com.ads.yetigame.model.World;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;


public class GameMenuScreen implements Screen {
    private Stage stage;
    private ImageActor backGround;
    private ImageActor informationButton;
    private ImageActor SoundMutedButton;
    private ImageActor SoundButton;
    private ImageActor championsButton;
    private ImageActor playButton;
    private ImageActor exitButton;
    private Music newMusic;
    public String resolvePath(String fileName) {
        return String.format(
                Gdx.app.getType() == Application.ApplicationType.Android ?
                        fileName :
                        String.format("android/assets/%s", fileName)
        );
    }

    public GameMenuScreen(SpriteBatch batch) {
        newMusic = Gdx.audio.newMusic(Gdx.files.internal(resolvePath("2048/Boom.mp3")));
        newMusic.setLooping(true);
        backGround = new ImageActor(new Texture(resolvePath("2048/main-bg.png")), 0, 0);
        informationButton = new ImageActor(new Texture(resolvePath("2048/info-r-r.png")), 170, 10);
        informationButton.addListener(new MoveToInfo());
        SoundMutedButton = new ImageActor(new Texture(resolvePath("2048/nsound.png")), 60, 900);
        SoundMutedButton.addListener(new ClickListener() {
                    public void clicked(InputEvent event, float x, float y) {newMusic.stop();
                    }
                });
        SoundButton = new ImageActor(new Texture(resolvePath("2048/SoundActivated.png")), 10, 900);
        SoundButton.addListener(new ClickListener() {
                    public void clicked(InputEvent event, float x, float y) {newMusic.play();

                    }
                });
        championsButton = new ImageActor(new Texture(resolvePath("2048/champions.png")), 550, 900);
        championsButton.addListener(new MoveToSettingsMenu());
        playButton = new ImageActor(new Texture(resolvePath("2048/p-btn.png")), 10, 330);
        playButton.addListener(new MoveToMainGamescreen());
        exitButton = new ImageActor(new Texture(resolvePath("2048/e-btn.png")), 330, 330);
        exitButton.addListener(new Exition());
      /* newMusic = Gdx.audio.newMusic(Gdx.files.internal("android/assets/2048/Boom.mp3"));
        newMusic.setLooping(true);
        newMusic.play();*/
        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);
        stage.addActor(backGround);
        stage.addActor(informationButton);
        stage.addActor(SoundButton);
        stage.addActor(championsButton);
        stage.addActor(exitButton);
        stage.addActor(playButton);
        stage.addActor(SoundMutedButton);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

}