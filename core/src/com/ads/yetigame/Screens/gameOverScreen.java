package com.ads.yetigame.Screens;

import com.ads.yetigame.Controller.Exition;
import com.ads.yetigame.Controller.MoveToMainGamescreen;
import com.ads.yetigame.Controller.MoveToMainMenu;
import com.ads.yetigame.Controller.MoveToSettingsMenu;
import com.ads.yetigame.View.ImageActor;
import com.ads.yetigame.model.World;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.util.logging.FileHandler;

/**
 * Created by 04k1103 on 23.04.2016.
 */
public class gameOverScreen implements Screen {
    private Stage stage;
    public World world;
    private ImageActor backGround;
    private ImageActor backToMainMenuButton;
    private ImageActor championsButton;
    private ImageActor playAgainButton;
    private ImageActor exitButton;
    public ScoreFontActor score;
    public ScoreFontActor highscore;

    public String resolvePath(String fileName) {
        return String.format(
                Gdx.app.getType() == Application.ApplicationType.Android ?
                        fileName :
                        String.format("android/assets/%s", fileName)
        );
    }

    public gameOverScreen(SpriteBatch batch) {
        score = new ScoreFontActor(true, 160, 700, 72, Color.WHITE, "shrift.ttf");
        highscore = new ScoreFontActor(false, 160, 380, 32, Color.BLACK, "shrift.ttf");
        backGround = new ImageActor(new Texture(resolvePath("2048/Lose.png")), 0, 0);
        backToMainMenuButton = new ImageActor(new Texture(resolvePath("2048/GOS-menu.png")), 150, 300);
        backToMainMenuButton.addListener(new MoveToMainMenu());
        championsButton = new ImageActor(new Texture(resolvePath("2048/GOS-records.png")), 150, 400);
        championsButton.addListener(new MoveToSettingsMenu());

        playAgainButton = new ImageActor(new Texture(resolvePath("2048/GOS-againButton.png")), 150, 500);

        playAgainButton.addListener(new MoveToMainGamescreen());
        exitButton = new ImageActor(new Texture(resolvePath("2048/GOS-exition.png")), 150, 200);
        exitButton.addListener(new Exition());
        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);
        stage.addActor(backGround);
        stage.addActor(backToMainMenuButton);
        stage.addActor(championsButton);
        stage.addActor(playAgainButton);
        stage.addActor(exitButton);
        stage.addActor(score);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }
}