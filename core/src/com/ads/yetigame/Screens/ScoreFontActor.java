package com.ads.yetigame.Screens;

import com.ads.yetigame.YetiGame;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by 04k1103 on 30.04.2016.
 */
public class ScoreFontActor extends Actor {
    private String str;
    private BitmapFont font;
    private FreeTypeFontGenerator generator;
    private FreeTypeFontGenerator.FreeTypeFontParameter parameter;
    private GlyphLayout layout;
    private boolean score;

    public ScoreFontActor(boolean score, float posX, float posY, int size, Color color, String fontName) {
        generator = new FreeTypeFontGenerator(Gdx.files.internal("android\\assets\\fonts\\"+ fontName));
        parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = size;
        parameter.color = color;
        font = new BitmapFont();
        this.score = score;
        font = generator.generateFont(parameter);
        generator.dispose();
        setPosition(posX, posY);
        layout = new GlyphLayout();
    }

    @Override
    public void draw(Batch batch, float alpha) {
        if (score) layout.setText(font, String.format("%d", YetiGame.getInstance().Score));
        else layout.setText(font, String.format("%d", YetiGame.getInstance().HighScore));
        font.draw(batch, layout, (Gdx.graphics.getWidth()-layout.width)/2, getY());
    }
}

