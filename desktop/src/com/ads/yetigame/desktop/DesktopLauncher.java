package com.ads.yetigame.desktop;


import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.ads.yetigame.YetiGame;


public class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        System.setProperty("user.name", "EnglishWords");

        config.title = "2048";
        config.width = 640;
        config.height = 1136;
        new LwjglApplication(YetiGame.getInstance(), config);
    }
}
